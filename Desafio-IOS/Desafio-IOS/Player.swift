//
//  Player.swift
//  DesafioIOS
//
//  Created by Brenno Hayden on 6/19/15.
//  Copyright (c) 2015 Brenno Hayden. All rights reserved.
//

import Foundation

class Player : NSObject {
    
    var id: Int
    
    var name : String
    
    var avatarUrl : String
    
    var url: String
    
    var twitter: String
    
    var website: String
    
    init(id: Int, name: String, avatarUrl: String, url : String, twitter: String, website: String) {
        
        self.id = id
        
        self.name = name
        
        self.avatarUrl = avatarUrl
        
        self.url = url
        
        self.twitter = twitter
        
        self.website = website
        
    }
    
}