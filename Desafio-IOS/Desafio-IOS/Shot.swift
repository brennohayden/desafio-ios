//
//  Shot.swift
//  DesafioIOS
//
//  Created by Brenno Hayden on 6/19/15.
//  Copyright (c) 2015 Brenno Hayden. All rights reserved.
//

import Foundation

class Shot : NSObject {
    
    var id: Int
    
    var title : String
    
    var descrip : String
    
    var height : Int!
    
    var width : Int!
    
    var likesCount : Int
    
    var commentsCount : Int!
    
    var reboundsCount : Int!
    
    var url : String!
    
    var shortUrl : String!
    
    var imgUrl : String
    
    var imgTeaserUrl : String!
    
    var image400Url : String!
    
    var player : Player
    
    init(id: Int, title: String, descrip: String,  likesCount: Int, imgUrl: String, url: String, shortUrl: String, player: Player) {
        //init(id: Int, title: String, descrip: String, height: Int, width: Int, likesCount: Int, commentsCount: Int, reboundsCount: Int, url :String, shortUrl : String, imgUrl: String, imgTeaserUrl: String, image400Url: String, player: Player) {
        
        self.id = id
        
        self.title = title
        
        self.descrip = descrip
        
        //        self.height = height
        
        //        self.width = width
        
        self.likesCount = likesCount
        
        //        self.commentsCount = commentsCount
        //
        //        self.reboundsCount = reboundsCount
        //
        self.url = url
        //
        self.shortUrl = shortUrl
        //
        self.imgUrl = imgUrl
        //
        //        self.imgTeaserUrl = imgTeaserUrl
        //
        //        self.image400Url = image400Url
        //
        self.player = player
        
    }
    
}