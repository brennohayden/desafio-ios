//
//  DetailsShotView.swift
//  DesafioIOS
//
//  Created by Brenno Hayden on 6/19/15.
//  Copyright (c) 2015 Brenno Hayden. All rights reserved.
//

import UIKit
import Foundation
import Haneke

extension String {
    var html2String:String {
        return NSAttributedString(data: dataUsingEncoding(NSUTF8StringEncoding)!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:NSUTF8StringEncoding], documentAttributes: nil, error: nil)!.string
    }
}

class DetailsShotView: UIViewController {
    
    
    @IBOutlet weak var imgShot: UIImageView!
    
    @IBOutlet weak var titleShot: UILabel!
    
    @IBOutlet weak var viewShot: UILabel!
    
    @IBOutlet weak var avatarPlayer: UIImageView!
    
    @IBOutlet weak var namePlayer: UILabel!
    
    @IBOutlet weak var descripShot: UILabel!
    
    var shot : Shot!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println(shot.descrip)
        
        imgShot.hnk_setImageFromURL(NSURL(string: shot.imgUrl)!)
        
        titleShot.text = shot.title
        
        viewShot.text = String(shot.likesCount)
        
        avatarPlayer.hnk_setImageFromURL(NSURL(string: shot.player.avatarUrl)!)
        
        avatarPlayer.layer.borderWidth = 2.0;
        
        avatarPlayer.layer.cornerRadius = avatarPlayer.frame.size.height/2
        
        avatarPlayer.clipsToBounds = true
        
        namePlayer.text = shot.player.name
        
        descripShot.text = shot.descrip.html2String
        
        descripShot.lineBreakMode = .ByWordWrapping
        
        descripShot.numberOfLines = 0
        
        descripShot.sizeToFit()
        
    }
    
    @IBAction func twitterPress(sender: AnyObject) {
        
        let player = self.shot.player
        
        if(Reachability.isConnectedToNetwork()) {
            UIApplication.sharedApplication().openURL(NSURL(string:"https://www.twitter.com/" + player.twitter)!)
        }else {
            MyAlert.alerta("Sem Conexão", title: "Alerta", button: "Voltar")
        }
    }
    
    @IBAction func fbPress(sender: AnyObject) {
        MyAlert.alerta("Sem funcionalidade", title: "Alerta", button: "Voltar")
        
    }
    
    @IBAction func gramPress(sender: AnyObject) {
        MyAlert.alerta("Sem funcionalidade", title: "Alerta", button: "Voltar")
        
    }
    
    @IBAction func webPress(sender: AnyObject) {
        if(Reachability.isConnectedToNetwork()) {
            
            UIApplication.sharedApplication().openURL(NSURL(string: shot.url)!)
        }else {
            MyAlert.alerta("Sem Conexão", title: "Alerta", button: "Voltar")
        }
    }
    
    @IBAction func sharePress(sender: AnyObject) {
        if(Reachability.isConnectedToNetwork()) {
            let textToShare = "Eu gostei do Shot \(shot.title) #DribbbleAPI #DesafioBrenno"
            
            if let shortUrl = NSURL(string: shot.shortUrl)
            {
                
                let objectsToShare = [textToShare, shortUrl]
                
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
                
                self.presentViewController(activityVC, animated: true, completion: nil)
            }
            
        }else {
            MyAlert.alerta("Sem Conexão", title: "Alerta", button: "Voltar")
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
