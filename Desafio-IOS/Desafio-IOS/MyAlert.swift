//
//  MyAlert.swift
//  DesafioIOS
//
//  Created by Brenno Hayden on 6/19/15.
//  Copyright (c) 2015 Brenno Hayden. All rights reserved.
//

import Foundation
import UIKit

public class MyAlert {
    
    
    class func alerta(message:String, title:String, button:String) {
        
        let alert = UIAlertView()
        alert.title = title
        alert.message = message
        alert.addButtonWithTitle(button)
        alert.show()
        
    }
}

