//
//  DataCell.swift
//  DesafioIOS
//
//  Created by Brenno Hayden on 6/18/15.
//  Copyright (c) 2015 Brenno Hayden. All rights reserved.
//

import  UIKit

class Datacell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var views: UILabel!
    
    @IBOutlet weak var twitter: UIButton!
    
    @IBOutlet weak var facebook: UIButton!
    
    @IBOutlet weak var instagram: UIButton!
    
    @IBOutlet weak var web: UIButton!
    
    var shot: Shot!
    
    override func prepareForReuse() {
        
        imgView.hnk_cancelSetImage()
        
        imgView.image = nil
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func twiiterPress(sender: AnyObject) {
        println("twiiterPress")
        
        let player = self.shot.player
        
        if(Reachability.isConnectedToNetwork()) {
            UIApplication.sharedApplication().openURL(NSURL(string: "https://twitter.com/" + player.twitter)!)
        }else {
            MyAlert.alerta("Sem Conexão", title: "Alerta", button: "Voltar")
        }
    }
    
    @IBAction func icon2Press(sender: AnyObject) {
        
        MyAlert.alerta("Sem funcionalidade", title: "Alerta", button: "Voltar")
        
    }
    @IBAction func icon3Press(sender: AnyObject) {
        
        MyAlert.alerta("Sem funcionalidade", title: "Alerta", button: "Voltar")
    }
    
    @IBAction func webPress(sender: AnyObject) {
        
        if(Reachability.isConnectedToNetwork()) {
            
            UIApplication.sharedApplication().openURL(NSURL(string: shot.url)!)
        }else {
            MyAlert.alerta("Sem Conexão", title: "Alerta", button: "Voltar")
        }
    }
    
    
    
}
