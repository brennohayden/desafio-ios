//
//  ViewController.swift
//  DesafioIOS
//
//  Created by Brenno Hayden on 6/18/15.
//  Copyright (c) 2015 Brenno Hayden. All rights reserved.
//

import UIKit
import Alamofire
import Haneke

class ViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableview: UITableView!
    
    let URL = "http://api.dribbble.com/shots/popular?page="
    
    // numero da paginas
    var page = 1
    
    // total de paginas
    
    var pages = 1000000
    
    //total de shots
    var total = 0
    
    //lista de shots
    var shots = [Shot]()
    
    //shots por pagina
    var pageSize = 15
    
    //cell
    let cellIdentifier = "cell"
    
    //loop infinite
    var loading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        
        self.tableview.backgroundColor = UIColor.whiteColor()
        
        if(Reachability.isConnectedToNetwork()) {
            
            self.loadPopularShots()
            
        }else {
            MyAlert.alerta("Sem Conexão", title: "Alerta", button: "Voltar")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //  request no servidor
    
    func loadPopularShotsSync(completionHandler: ([Shot]?, NSError?) -> ()) -> () {
        
        if (!self.loading) {
            
            self.setLoadingState(true)
            
            Alamofire.request(.GET, URL + String(page)).responseJSON {
                (request, response, json, error) in
                
                if(error != nil)  {
                    
                    completionHandler(nil, error)
                    
                } else {
                    
                    let jsonObj = JSON(json!)
                    
                    //parser cabeçalho
                    let pages = jsonObj["pages"].int!
                    
                    let tshots = jsonObj["total"].int!
                    
                    self.pages = pages
                    
                    self.total = tshots
                    
                    let listShots = jsonObj["shots"];
                    
                    for(index: String, shotObjt: JSON) in listShots {
                        
                        var playerObject = shotObjt["player"]
                        
                        var player = Player(id: playerObject["id"].intValue, name: playerObject["name"].stringValue, avatarUrl: playerObject["avatar_url"].stringValue,
                            url : playerObject["url"].stringValue, twitter: playerObject["username"].stringValue, website: playerObject["website_url"].stringValue)
                        
                        
                        var shot = Shot(id: shotObjt["id"].intValue, title: shotObjt["title"].stringValue, descrip: shotObjt["description"].stringValue, likesCount: shotObjt["likes_count"].intValue, imgUrl: shotObjt["image_url"].stringValue, url: shotObjt["url"].stringValue, shortUrl: shotObjt["short_url"].stringValue,  player: player)
                        
                        self.shots.append(shot)
                        
                    }
                    
                    completionHandler(self.shots, error)
                }
                
            }
        }
    }
    
    func loadPopularShots () {
        
        loadPopularShotsSync { (shots, error) in
            
            //tratar
            if(shots?.count > 0) {
                
                self.shots = shots!
                
                self.tableView.reloadData()
                
                self.setLoadingState(false)
                
                if(self.page <= self.pages-1) {
                    self.page++
                }else {
                    self.setLoadingState(true)
                    
                }
                
            }else {
                println("lista vazia")
            }
            
        }
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return shots.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView!.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! Datacell
        
        var shot = shots[indexPath.row]
        
        cell.shot = shot
        //image cash
        cell.imgView.hnk_setImageFromURL(NSURL(string: shot.imgUrl)!)
        
        cell.title.text = shot.title
        
        cell.views.text = String(shot.likesCount)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = self.tableView!.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! Datacell
        
        var shot = self.shots[indexPath.row]
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        
        var dest : DetailsShotView = mainStoryboard.instantiateViewControllerWithIdentifier("DetailsShotView") as! DetailsShotView
        
        dest.shot = self.shots[indexPath.row]
        
        self.navigationController?.pushViewController(dest, animated: false)
        
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let currentOffset = scrollView.contentOffset.y
        
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 40 {
            
            self.loadPopularShots()
            
        }
    }
    
    func setLoadingState(loading:Bool) {
        
        self.loading = loading
        
    }
    
}

